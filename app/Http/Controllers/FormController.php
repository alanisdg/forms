<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class FormController extends Controller
{
    public function new(){
        return Inertia::render('Form', [
            'user' => Auth::user()
        ]);
    }

    public function list(){
        return  Form::all();
    }

    public function store(Request $request){
        Form::create([
            "name"=>$request->name
        ]);
    }
}
