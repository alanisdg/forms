<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{

    public $successStatus = 200;
    public $successCreated = 201;
    public $notAuthorized = 401;

    public function login(Request $request) {
        if(Auth::attempt(['email' => $request['email'],
            'password' => $request['password']],
            $request['remember'])) {
            $user = User::find(Auth::user()->id);
            $user->token = $user->createToken("formsToken");
            return response()->json($user, $this->successStatus);
        } else {
            $data = array(
                'message' => 'Por favor verifique su usuario y contraseña',
            );
            return response()->json($data, $this->notAuthorized);
        }
    }
}
