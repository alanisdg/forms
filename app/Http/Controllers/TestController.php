<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class TestController extends Controller
{
    public function test(){
        $all = array([
            'hola'=>'vites'
        ]);

        return Inertia::render('Users', [
            'users' => $all
        ]);
    }
}
